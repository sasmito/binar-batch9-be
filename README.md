# binar-batch9-be

## Project Binar Student Class BE

Members:
- Muhammad Sasmito Adi Wibowo
- Ricky Setiawan
- Wachid Baharudin
- Gigih JR
- Rudi Sarwiyana

ERD: 
- https://www.draw.io/#G1A3fGQvyNrvmEJ07XhLKJP_xE-nFfawVt

## MATERI

### Software Design Principles
-   [S.O.L.I.D principles](https://robots.thoughtbot.com/back-to-basics-solid)
-   [Software Arcitecture](https://sourcemaking.com/)

### Clean code
-   https://github.com/rubocop-hq/ruby-style-guide
-   https://github.com/uohzxela/clean-code-ruby

### Git
-   [Book Git](https://books.goalkicker.com/GitBook/)
-   [Git Intro](https://git-scm.com/book/id/v1/Memulai-Git-Tentang-Version-Control)

### Ruby
-   [Book Ruby Basic](https://launchschool.com/books/ruby/read/introduction)
-   [Book Ruby OOP(Object Oriented Programming)](https://launchschool.com/books/oo_ruby/read/introduction)
-   [Ruby Code Convention](https://github.com/rubocop-hq/ruby-style-guide)
-   [Ruby Monk](https://rubymonk.com/)
-   [Hackerrank Ruby](https://www.hackerrank.com/domains/ruby)
-   [Codecademy Ruby](https://www.codecademy.com/learn/learn-ruby)
-   [Awesome Ruby Project](http://awesome-ruby.com/)
-   [IdRails](http://www.idrails.com/series)
-   [Railscasts](http://railscasts.com/)

### Ruby on Rails
-   [Ruby on Rails Book](https://www.railstutorial.org/book)
-   [Ruby on rails api](https://scotch.io/tutorials/build-a-restful-json-api-with-rails-5-part-one)

### Postgresql
-   [Book Postgresql](https://books.goalkicker.com/PostgreSQLBook/)
-   [Setup Postgresql](https://www.digitalocean.com/community/tutorials/how-to-install-and-use-postgresql-on-ubuntu-16-04)

### Deploy
-   [Deploy Heroku](https://devcenter.heroku.com/articles/getting-started-with-rails5)

### Unit testing
-   https://dzone.com/articles/top-8-benefits-of-unit-testing
-   https://github.com/muhammadsasmito/rails-workshop
-   https://github.com/rspec/rspec-rails
-   https://github.com/colszowka/simplecov
-   https://github.com/thoughtbot/factory_bot_rails
-   https://github.com/thoughtbot/factory_bot/blob/master/GETTING_STARTED.md
