class Hewan
    def initialize
        puts "Initialized"
    end
    #kaki
    def set_kaki(k)
        @kaki = k
    end
    #alat nafas
    def set_alat_nafas(a)
        @alat_nafas = a
    end

    def show_info
        puts "berkaki #{@kaki} dan bernafas dengan #{@alat_nafas}"
    end
end
class Kucing < Hewan
end
class Ayam < Hewan
end
class Ikan < Hewan
    def show_info
        puts "ikan tidak berkaki, ia bersirip dan ia berenang dalam otakmu"
    end
end
