Rails.application.routes.draw do
  root to: 'home#index'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  resources :likes
  resources :posts
  resources :authors
  get '/penulis', to: 'authors#index'
end
