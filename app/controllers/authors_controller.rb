class AuthorsController < ApplicationController
  skip_before_action :verify_authenticity_token  
  before_action :author_params, except: [:index, :show]

  def index
    @author = Author.all
    render json: @author.present? ? { status: :ok, result: @author, errors: nil } : { status: :unprocessable_entity, result: @author, errors: nil }
  end
  
  def show
    @author = Author.find_by(id: params[:id])
    render json: @author.present? ? { status: :ok, result: @author, errors: nil } : { status: :unprocessable_entity, result: @author, errors: nil }
  end

  def create
    @author = Author.new(author_params)
    render json: @author.save ? { status: :created, result: @author, errors: nil } : { status: :unprocessable_entity, result: nil, errors: ["Something went wrong"] }
  end

  private

  def author_params
    params.require(:author).permit(:name, :twitter, :about)
  end
end
