class PostsController < ApplicationController
    skip_before_action :verify_authenticity_token
    # protect_from_forgery with: :exception
    before_action :set_post, only: [:update, :show, :destroy]

    def index
        @posts = Post.all
        render json: { status: :ok, result: @posts, error: nil }
        # if @posts
        #     render json: { status: :ok, result: @posts, error: nil }
        # else
        #     render json: { status: :not_found, result: @posts, error: nil }
        # end
    end

    def show
        if @post.present?
            render json: { status: :ok, result: @post, error: nil }
        else
            render json: { status: :not_found, result: @posts, error: nil }
        end
    end

    def create
        @post = Post.new(post_params)
        # @like = Like.new(like_params)
        if @post.save
            render json: { status: :created, result: @post, error: nil }
        else
            render json: { status: :unprocessable_entity, result: @post, error: nil }
        end
    end

    def update
        if @post.present?
            @post.update(post_params)
            head :no_content
        else
            render json: { status: :unprocessable_entity, result: @post, error: nil }
        end
    end

    def destroy
        if @post.present?
            @post.destroy!
            head :no_content
        else
            render json: { status: :unprocessable_entity, result: @post, error: nil }
        end
    end

    private
        def set_post
            @post = Post.find_by(id: params[:id])
            # if !@post.present?
            #     render json: { status: :not_found, result: @posts, error: nil }
            # end
        end

        def set_author_id
            @author_id = Post.find_by(params[:author_id]).id
        end

        def post_params
            params.require(:post).permit(:name, :body, :author_id, :category_id)
        end
end
