class LikesController < ApplicationController
    skip_before_action :verify_authenticity_token
    before_action :set_like, only: [:update, :show, :destory]

    def index
        @likes = Like.all
        render json: { status: :ok, result: @likes, error: nil }
    end

    def show
        render json: { status: :ok, result: @like, error: nil }
    end

    def create
        @like = Like.new(like_params)

        if @author_id
            if @like.save
                render json: { status: :created, result: @like, error: nil }
            else
                render json: { status: :unprocessable_entity, result: @like, error: nil }
            end
        else
            render json: { status: :unprocessable_entity, result: @like, error: nil }
        end\
    end

    def update
        @like.update(like_params)
        head :no_content
    end

    def destroy
        @like.destroy
        head :no_content
    end

    private
        def set_like
            @like = Like.find(params[:id])
        end

        def set_author_id
            @author_id = Post.find_by(params[:author_id]).id
        end


        def like_params
            params.require(:likes).permit(:author_id, :post_id)
        end


end
