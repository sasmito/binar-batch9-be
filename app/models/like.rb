class Like < ApplicationRecord
    belongs_to :post
    belongs_to :author

    validates_presence_of :author_id, :post_id
end
