class Post < ApplicationRecord
    has_many :likes

    validates_presence_of :name, :body, :author_id, :category_id
end
