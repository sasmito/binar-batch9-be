class Author < ApplicationRecord
  has_many :books
  validates :name, :about, :twitter, presence: true
end
