# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
author = Author.new
author.name = "Rowling"
author.twitter = "@row"
author.gitlab = "@row"
author.about = "ahahahhah"
author.save

author = Author.new
author.name = "J.K Rowling"
author.twitter = "@row1"
author.gitlab = "@row1"
author.about = "wkkwkwk"
author.save

category = Category.new
category.name = "Buku"
category.save

post = Post.new
post.name = "Harry Potter"
post.body = "Voldemort mati kepentok sapu"
post.author_id = 1
post.category_id = 1
post.save

post = Post.new
post.name = "Harry Putter"
post.body = "Voldemort mati kepentok sapu"
post.author_id = 2
post.category_id = 1
post.save

like = Like.new
like.author_id = 1
like.post_id = 2
like.save