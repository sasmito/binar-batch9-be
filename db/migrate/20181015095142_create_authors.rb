class CreateAuthors < ActiveRecord::Migration[5.2]
  def change
    create_table :authors do |t|
      t.string :name
      t.string :twitter
      t.string :gitlab
      t.text :about

      t.timestamps
    end
  end
end
